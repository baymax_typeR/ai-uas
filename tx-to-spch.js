function sl_pnmo() {
    responsiveVoice.speak(
        "Penyebaran penyakit nyumonia melalui droplet pernapasan baik itu batuk atau bersin. Gejala nya antara lain batuk berdahak atau bernanah, demam, menggigil, dan kesulitan bernapas. Untuk perawatan, Antibiotik dan Penisilin dapat mengobati berbagai jenis nyumonia dan beberapa jenis nyumonia dapat dicegah dengan vaksin. Dapat dilakukan perawatan pendukung seperti terapi oksigen, terapi rehidrasi oral, dan Cairan IV.",
        "Indonesian Female", {
            pitch: 1,
            rate: 1,
            volume: 20
        }
    );
}

function play_pnmo() {
    responsiveVoice.speak(
        "Nyumonia adalah penyakit yang menimbulkan peradangan pada kantung udara di salah satu atau kedua paru-paru, yang dapat berisi cairan. Untuk penyebaran, gejala, dan perawatan, tekan selengkapnya.",
        "Indonesian Female", {
            pitch: 1,
            rate: 1,
            volume: 20
        }
    );
}

function sl_cmpk() {
    responsiveVoice.speak(
        "Penyebaran penyakit campak melalui droplet pernapasan baik itu batuk atau bersin, melalui kontak dengan permukaan yang terkontaminasi, melalui kontak kulit, serta dari Ibu ke bayi dalam proses mengandung, persalinan, atau menyusui. Gejala nya antara lain nyeri otot, demam, malaise, kehilangan selera makan, flu, ruam kulit, batuk kering, mata merah, pembengkakan kelenjar getah bening, sakit kepala, sakit tenggorokan, serta sensitif terhadap cahaya. Untuk perawatan, tidak ada pengobatan untuk menyembuhkan infeksi campak yang sudah terjadi, tetapi penurun demam atau vitamin A yang dijual bebas dapat meringankan gejala.",
        "Indonesian Female", {
            pitch: 1,
            rate: 1,
            volume: 20
        }
    );
}

function play_cmpk() {
    responsiveVoice.speak(
        "Campak adalah penyakit yang menyebar melalui udara dengan droplet hasil pernafasan yang dihasilkan dari batuk atau bersin. Untuk penyebaran, gejala, dan perawatan, tekan selengkapnya.",
        "Indonesian Female", {
            pitch: 1,
            rate: 1,
            volume: 20
        }
    );
}

function sl_dsnt() {
    responsiveVoice.speak(
        "Penyebaran penyakit disentri melalui makanan atau air yang terkontaminasi. Gejalanya antara lain diare berdarah, nyeri perut, demam, dan malaise. Untuk perawatan, dapat dilakukan dengan terapi menggunakan cairan baik oral maupun intravena. Obat antinyeri untuk mengurangi nyeri dan ketidaknyamanan yang dirasakan pengidapnya. Obat untuk meredakan kram perut dan diare. Serta obat antibiotik sesuai petunjuk dokter.",
        "Indonesian Female", {
            pitch: 1,
            rate: 1,
            volume: 20
        }
    );
}

function play_dsnt() {
    responsiveVoice.speak(
        "Disentri adalah penyakit yang disebabkan oleh bakteri Shigella, Salmonella, E Koli, atau Campylobacter yang menyebar melalui makanan atau air yang terkontaminasi. Untuk penyebaran, gejala, dan perawatan, tekan selengkapnya.",
        "Indonesian Female", {
            pitch: 1,
            rate: 1,
            volume: 20
        }
    );
}

function play_dbd() {
    responsiveVoice.speak(
        "Demam berdarah adalah penyakit yang diakibatkan oleh salah satu dari empat virus deng gi yang dibawa oleh nyamuk Aedes aegypti. Untuk penyebaran, gejala, dan perawatan, tekan selengkapnya.",
        "Indonesian Female", {
            pitch: 1,
            rate: 1,
            volume: 20
        }
    );
}

function sl_dbd() {
    responsiveVoice.speak(
        "Penyebaran penyakit demam berdarah melalui nyamuk Aedes aegypti. Gejalanya antara lain, demam, kehilangan selera makan, mual atau muntah, bintik merah atau ruam pada kulit, serta sakit kepala. Untuk perawatan, dapat dilakukan dengan tirah baring atau bet res. Usahakan penderita demam berdarah bisa beristirahat di tempat tidur sebanyak mungkin. Atasi suhu tubuh yang tinggi dengan cara kompres tubuh penderita dengan air suam-suam kuku jika suhu badan tetap tinggi.",
        "Indonesian Female", {
            pitch: 1,
            rate: 1,
            volume: 20
        }
    );
}

// responsiveVoice.OnVoiceReady = function () {
//     responsiveVoice.speak(
//         "Tekan Mulai untuk memulai diaknosa, sembilan puluh persen akurat, dijamin!.",
//         "Indonesian Female", {
//             pitch: 1,
//             rate: 1,
//             volume: 20
//         }
//     );
// };

function stop() {
    responsiveVoice.cancel();
}

function pause() {
    responsiveVoice.pause();
}

function resume() {
    responsiveVoice.resume();
}
